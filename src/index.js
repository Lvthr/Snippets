import React from 'react';
import ReactDOM from 'react-dom';
import App from "./js/App.js";
import './fonts/Courier Prime Sans.ttf';
import './css/index.css';

ReactDOM.render(
  <App/>,
  document.getElementById('root')
);