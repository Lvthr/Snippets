import React from 'react';
import { Component } from 'react';

import copy from "copy-to-clipboard";  

import '../css/App.css';
import '../css/Global.css';
import SearchBar from './components/SearchBar.js';
import Snippet from './components/Snippet.js';
import SnippetDetail from './components/SnippetDetail.js';
import NewSnippet from './components/NewSnippet.js';
import Settings from './components/Settings.js';
import SnippetService from './services/SnippetService.js';
const snippetservice = new SnippetService();
import ShortcutService from './services/ShortcutService.js';
const shortcuts = new ShortcutService();
var ipcRenderer = window.require('electron').ipcRenderer;

class App extends Component{
    constructor(props) {
        super(props);
        this._handleKeyDown = this._handleKeyDown.bind(this);
        this._update = this._update.bind(this);
        this._selectSnippet = this._selectSnippet.bind(this);
        
        document.addEventListener("keydown", this._handleKeyDown);
        let snippets = snippetservice.getCurrentSnippets("") 
        let selectedIndex = 0
        if(snippets.length <= 0) {
            selectedIndex = undefined
        }
        this.state = {
            snippets: snippets,
            query: "",
            mode: 'main', // options: "main", "new", "settings"
            selectedIndex: selectedIndex
        };
        console.log(ipcRenderer.sendSync('app-loaded', 'done'));

    }

    render() {
        if(!this.state.snippets) {
            this.setState({snippets: snippetservice.getCurrentSnippets(this.state.query)});
            return null;
        }

        let view;
        if(this.state.mode == "main") {
            view = (
                <div className='row'>
                    {/* listView goes here */}
                    {this.listView()}
                    {/* detailView goes here */}
                    {this.detailView()}
                </div>
            );
        } else if(this.state.mode == "new") {
            view = this.newSnippetForm();
        } else if(this.state.mode == "settings") {
            view = this.settingsView();
        } else {
            view = (
                <div>
                    Invalid view mode test
                </div>
            );
        }

        return (
            <div className="container-fluid m-0 p-0" style={{'margin': '0'}}>
                {/* searchBar goes here */}
                {this.searchBar()}
                {/* newSnippetButton goes here */}
                {this.newSnippetButton()}
                <div className='container-fluid' style={{'margin-right': '0px'}}>
                    {view}
                </div>
                {/* possible errorMessage goes here */}
                {this.errorMessage()}
            </div>  
        );
    }

    settingsView() {
        return (
            <Settings onImport={() => this._update(this.state.query)}/>
        );
    }

    searchBar() {
        return (
            <div className='navbar navbar-expand-md p-0 border-bottom main-navbar'>
                <SearchBar onQuery={this._update} onSettings={() => this.setState({mode: "settings"})} onMain={() => this.setState({mode: 'main'})}/>   
            </div>
        );
    }

    newSnippetButton() {
        return (
            <div className='navbar p-1 border-bottom newbutton-navbar w-100'>
                <div className='ml-auto mr-3'>
                    <button 
                        className='osxbtn' 
                        onClick={(e) => {
                                this.setState({mode: "new"});
                            }}>
                            + Add snippet
                    </button>
                </div>
            </div>
        );
    }

    detailView() {
        return (
            <div className='col-7 p-0'>
                {this.state.selectedIndex != undefined ? 
                    <SnippetDetail 
                        snippet={this.state.snippets[this.state.selectedIndex]} 
                        onChange={() => {this._update(this.state.query)}}/> 
                    : null}
            </div>
        );
    }

    listView() {
        return (
            <div className='col-5 p-0 row-height'>
                <div className='scrollable'>
                    <div className='container-fluid mb-5'>
                        {this.state.snippets.map((elem, i) => {
                            return (
                                <Snippet 
                                    snippet={elem} 
                                    selected={this.state.selectedIndex == i} 
                                    index={i} 
                                    onClick={(snippet) => {
                                        this._selectSnippet(snippet, "select");
                                    }}/>
                            );
                        })}
                    </div>
                </div>
            </div>
        );
    }

    errorMessage() {
        if(this.state.error != undefined && this.state.error != null) {
            return (
                <div className='row mt-3 alert alert-danger'>
                    {this.state.error}
                </div> 
            );
        } else {
            return null;
        }
    }

    newSnippetForm() {
        return (
            <div className='row'>
                <NewSnippet 
                    onSave={() => { // Will be called whenever a snippet is saved
                        this.setState({
                            mode: "main",
                            query: this.state.query,
                            snippets: snippetservice.getCurrentSnippets(this.state.query),
                            selectedIndex: undefined
                        });
                        //this._update(this.state.query);
                    }} 
                    onCancel={() => this.setState({mode: "main"})} // Will be called whenever the cancel button is pressed
                    />
            </div>
        );
    } 

    _update(query) {
        if(query != undefined && query != null) {
            let snippets = snippetservice.getCurrentSnippets(query); 
            let selectedIndex = 0
            if(snippets.length <= 0) {
                selectedIndex = undefined
            }
            this.setState({
                mode: "main",
                query: query,
                snippets: snippets,
                selectedIndex: selectedIndex
            });
        } else {
            let snippets = snippetservice.getCurrentSnippets(this.state.query); 
            let selectedIndex = 0
            if(snippets.length <= 0) {
                selectedIndex = undefined
            }
            this.setState({
                mode: "main",
                snippets: snippets,
                selectedIndex: selectedIndex
            });
        }
    }

    _selectSnippet(data, type) {
        if(type == "increment") {
            let incrementor = data;
            let currentIndex = this.state.selectedIndex;
            if(currentIndex != undefined && currentIndex != null) {
                if(currentIndex + incrementor >= 0 && currentIndex + incrementor < this.state.snippets.length) {
                    this.setState({selectedIndex: currentIndex + incrementor});
                }
            } else {
                if(this.state.snippets.length > 0) {
                    this.setState({selectedIndex: 0});
                } else {
                    this.setState({selectedIndex: undefined});
                }
            }
        } else {
            let snippetToSelect = data;
            for(let i = 0; i < this.state.snippets.length; i++) {
                if(snippetToSelect.id == this.state.snippets[i].id) {
                    this.setState({selectedIndex: i});
                    break;
                }
            }
        }        
    }

    _handleKeyDown(e) {
        if(e.key == "ArrowDown") {
            // Select next snippet down in the list view. E.g.: snippets[selected_index + 1]
            this._selectSnippet(1, "increment");
        } else if(e.key == "ArrowUp") {
            // Select next snippet up in the list view. E.g.: snippets[selected_index - 1]
            this._selectSnippet(-1, "increment");
        } else if(e.key == "Enter") {
            // If a snippet is selected, copy snippet.content to clipboard and close app/window
            if(this.state.selectedIndex != undefined && this.state.selectedIndex != null) {
                let toClipboard = this.state.snippets[this.state.selectedIndex].content;
                copy(toClipboard);
                //window.close();
                ipcRenderer.send('close-app', 'Enter pressed');
            }
        } else if(e.key == 'Escape') {
            if(this.state.query != "") {
                document.getElementById('query').value = "";
                this.setState({query: ""});
                this._update(this.state.query);
            } else {
                ipcRenderer.send('close-app', 'esc-pressed');
            }
            
        } else {
            shortcuts.handleEvent(this, e);
        }
    }
}
export default App;