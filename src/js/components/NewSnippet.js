import React from 'react';
import { Component } from 'react';
import SnippetService from '../services/SnippetService.js';
const snippetservice = new SnippetService();

class NewSnippet extends Component{
    constructor(props) {
        super(props);
        this.state = {title: '', content: ''};
        this.contentChange = this.contentChange.bind(this);
        this.titleChange = this.titleChange.bind(this);
        this.saveSnippet = this.saveSnippet.bind(this);
        this.cancel = this.cancel.bind(this);
    }

    render() {
        return (
            <div className='container p-3'>
                <div className='form-group'>
                    <label for='title'>Snippet title</label>
                    <input
                    id='title'
                    type='text'
                    aria-describedby='titleHelp'
                    placeholder='Snippet title'
                    className='form-control'
                    value={this.state.title} 
                    onChange={this.titleChange}/>
                    <small id='titleHelp'>Give your snippet a title that's easy to remember!</small>
                </div>
                <div className='form-group'>
                    <label for='content'>Snippet content</label>
                    <textarea 
                        id='content'
                        type='text' 
                        className='form-control'
                        placeholder='Snippet content goes here!'
                        aria-describedby='contentHelp'
                        value={this.state.content} 
                        onChange={this.contentChange}/>
                    <small id='contentHelp'>This is the snippet's content that will be copied to your clipboard when selected</small>
                </div>
                <div className='row pt-2'>
                    <div className='col align-self-start p-0'>
                        <button className='osxbtn osxbtn-primary' onClick={this.saveSnippet}>Save snippet</button>
                    </div>
                    <div className='col align-self-end p-0'>
                        <div className='float-right'>
                            <button className='osxbtn' onClick={this.cancel}>Cancel</button>
                        </div>
                    </div>
                </div>
                {this.state.error ? 
                    <div className='row mt-3 alert alert-danger'>
                        {this.state.error}
                    </div> 
                    : null}
            </div>
        );
    }

    saveSnippet(e) {
        if(this.state.title != '' && this.state.content != '') {
            let snippet = {
                title: this.state.title,
                content: this.state.content
            };
            snippetservice.newSnippet(snippet, (err) => {
                if(err) {
                    this.setState({error: err});        
                } else {
                    this.props.onSave();
                }
            });
        } else {
            this.setState({error: 'Title and/or content is empty. Try again.'});
        }
    }

    cancel(e) {
        this.props.onCancel();
    }

    titleChange(e) {
        this.setState({title: e.target.value, error: undefined});
    }

    contentChange(e) {
        this.setState({content: e.target.value, error: undefined});
    }
}
export default NewSnippet;