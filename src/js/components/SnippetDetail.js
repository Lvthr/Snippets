import React from 'react';
import { Component } from 'react';
import '../../css/SnippetDetail.css';
import '../../css/Global.css';
import SnippetService from '../services/SnippetService.js';
const snippetservice = new SnippetService();

class SnippetDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {editmode: false, snippet: props.snippet, deleted: false};
        this._setEditMode = this._setEditMode.bind(this);
        this._titleChange = this._titleChange.bind(this);
        this._contentChange = this._contentChange.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.deleteSnippet = this.deleteSnippet.bind(this);
    }

    render() {
        if(this.state.snippet.id != this.props.snippet.id) {
            this.setState({snippet: this.props.snippet, editmode: false, deleted: false});
        }
        
        if(this.state.deleted == true) {
            return (
                <div className='container h-100 p-3 snippetdetail'>
                    <div className='row border rounded p-2 h5 mb-2 w-100' onClick={this._setEditMode}>Deleted</div>
                    <p className='row border rounded p-2 w-100 h-50' onClick={this._setEditMode}>
                        The snippet has been deleted
                    </p>
                </div>
            );
        } else if(this.state.editmode == true) {
            return (
                <div className='container h-100 p-3 snippetdetail'>
                    <input 
                        type='text' 
                        className='row border rounded p-2 h5 mb-2 w-100'
                        value={this.state.updatedSnippet.title} 
                        onChange={this._titleChange}/>
                    <textarea 
                        type='text' 
                        className='row border rounded p-2 w-100 h-50'
                        value={this.state.updatedSnippet.content} 
                        onChange={this._contentChange}/>
                    <div className='row pt-2'>
                        <div className='col-8 align-self-start p-0'>
                            <button className='osxbtn osxbtn-primary mr-2' onClick={this.saveChanges}>Save</button>
                            <button className='osxbtn' onClick={this._setEditMode}>Cancel</button> 
                        </div>
                        <div className='col-4 align-self-end p-0'>
                            <div className='float-right'>
                                <button className='osxbtn osxbtn-danger' onClick={this.deleteSnippet}>Delete</button>
                            </div>
                        </div>
                        {this.state.error ? 
                            <div className='row mt-3 alert alert-danger'>
                                {this.state.error}
                            </div> 
                        : null}
                    </div>
                </div>
            );
        } else {
            return (
                <div className='container h-100 p-3 snippetdetail'>
                    <div className='row border rounded p-2 h5 mb-2 w-100 bg-light'>{this.props.snippet.title}</div>
                    <p className='row border rounded p-2 w-100 h-50 bg-light'>
                        {this.props.snippet.content}
                    </p>
                    <div className='row pt-2'>
                        <div className='col align-self-start p-0'>
                            <button className='osxbtn osxbtn-primary' onClick={this._setEditMode}>Edit</button>
                        </div>
                    </div>
                    
                    {this.state.error ? 
                        <div className='row mt-3 alert alert-danger'>
                            {this.state.error}
                        </div> 
                    : null}
                </div>
            );
        }
    }

    _setEditMode(e) {
        let editmode = this.state.editmode;
        let newState = {editmode: !editmode};
        if(this.state.editmode == false) {
            newState.updatedSnippet = this.props.snippet;
        }
        this.setState(newState);
    }

    _titleChange(e) {
        let updatedSnippet = this.state.updatedSnippet;
        updatedSnippet.title = e.target.value;
        let newState = this.state;
        newState.updatedSnippet = updatedSnippet;
        newState.error = "";
        this.setState(newState);
    }

    _contentChange(e) {
        let updatedSnippet = this.state.updatedSnippet;
        updatedSnippet.content = e.target.value;
        let newState = this.state;
        newState.newSnippet = updatedSnippet;
        newState.error = "";
        this.setState(newState);
    }

    saveChanges(e) {
        console.log(this.state.updatedSnippet);
        snippetservice.updateSnippet(this.state.updatedSnippet, (err) => {
            if(err) {
                this.setState({error: err});
            } else {
                console.log('saved');
                this.setState({editmode: false, snippet: this.state.updatedSnippet});
                this.props.onChange();
            }
        });
    }

    deleteSnippet(e) {
        console.log('deleting', this.props.snippet)
        snippetservice.deleteSnippet(this.props.snippet, (err) => {
            console.log('deleted', err);
            if(err) {
                this.setState({error: err});
            } else {
                this.setState({deleted: true, editmode: false});
                this.props.onChange();
            }
        });
    }
}
export default SnippetDetail;