import React from 'react';
import { Component } from 'react';
import '../../css/Settings.css';
import ShortcutService from '../services/ShortcutService.js';
const shortcuts = new ShortcutService();
import ImportService from '../services/ImportService.js';
const importservice = new ImportService();
import SnippetService from '../services/SnippetService.js';
const snippetservice = new SnippetService();
var ipcRenderer = window.require('electron').ipcRenderer;

var remote = window.require('electron').remote;
const log = remote.require('electron-log');

const handleDragEnter = e => {
    e.preventDefault();
    e.stopPropagation();
};
const handleDragLeave = e => {
    e.preventDefault();
    e.stopPropagation();
};
const handleDragOver = e => {
    e.preventDefault();
    e.stopPropagation();
};


class Settings extends Component {
    constructor(props) {
        super(props);
        this.state = {}
        this._handleDrop = this._handleDrop.bind(this);
        this._handleSelect = this._handleSelect.bind(this);
        this._import = this._import.bind(this);
    }

    render() {
        return (
            <div className='row-height'>
                <div className='scrollable'>
                    <div className='container-fluid mx-2 mb-5 px-3 mt-3'>
                        <h5 className='align-center'>Shortcuts:</h5>
                        {shortcuts.getShortcuts().map((elem, i) => {
                            console.log(elem);
                            return(
                                <div className='row mx-0 px-0'>
                                    <div className='col-9'>
                                        <div>{elem.name}</div>
                                        <small>{elem.description}</small>
                                    </div>
                                    <div className='col-3'>
                                        <strong>{elem.readable_shortcut}</strong>
                                    </div>
                                </div>
                            );
                        })}
                        <h5 className='my-2'>Manage snippets</h5>
                        <div className='row my-2'>
                            <div className='col'>
                                <strong>Import snippets:</strong>
                                {this.state.error ? <div>{this.state.error}</div> : null}
                                <input type='file' id='fileinput' style={{'display': 'none'}} accept='application/json' onChange={this._handleSelect} multiple/>
                                <div className="upload-drop-zone"
                                    onDrop={e => this._handleDrop(e)}
                                    onDragOver={e => handleDragOver(e)}
                                    onDragEnter={e => handleDragEnter(e)}
                                    onDragLeave={e => handleDragLeave(e)}
                                    onClick={(e) => document.getElementById('fileinput').click()}>
                                        <div className='drop-zone-text'>Drag or click to import snippets by .json file!</div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <strong className="col-10">
                                Delete all currently saved snippets:
                            </strong>
                            <div className='col-2'>
                                <button className='osxbtn osxbtn-danger'>Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    _import(files) {
        let snippets_to_import = [];
        for(let i = 0; i < files.length; i++) {
            let file = files[i];

            if(file.type == "application/json") {
                importservice.readFile(file.path, (err, data) => {
                    if(err) {
                        console.log(err);
                    } else {
                        if(data.snippets && Array.isArray(data.snippets)) {
                            for(let j = 0; j < data.snippets.length; j++) {
                                if(data.snippets[j].title && data.snippets[j].content) {
                                    snippets_to_import.push({title: data.snippets[j].title, content: data.snippets[j].content});
                                }
                            }
                        }
                    }
                });
            } else {
                this.setState({error: 'One or more files are not of correct filetype. Files to import must be of type .json'});
            }
        }
        console.log(snippets_to_import);
        if(snippets_to_import.length > 0) {
            snippetservice.import(snippets_to_import, (err) => {
                if(err) {
                    console.log(err);
                } else {
                    this.props.onImport();
                }
            });
        }
    }

    _handleDrop(e) {
        e.preventDefault();
        e.stopPropagation();
        log.info(e.dataTransfer.files)
        this._import(e.dataTransfer.files);
        
        // this.setState({message: e});
    }

    _handleSelect(e) {
        this._import(e.target.files);
    }
}
export default Settings; // test