import React from 'react';
import { Component } from 'react';
import '../../css/SearchBar.css';
import logo from '../../../assets/icon.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCog, faCogs } from '@fortawesome/free-solid-svg-icons'

class SearchBar extends Component{
    render() {
        return(
            <div className="container-fluid">
                <div className="row w-100 align-items-center">
                    <div className="mx-0 px-1 text-center" style={{'width': '10%', 'left': '0px'}}>
                        <img src={logo} className="searchbar-logo rounded-circle" onClick={(e) => this.props.onMain()}/>
                    </div>
                    <div className="mx-0 px-0" style={{'width': '80%'}}>
                        <input 
                            className="searchbar py-3 px-0 w-100" 
                            type="text" 
                            id="query" 
                            placeholder='Search snippets by title'
                            onChange={(e) => {
                                this.props.onQuery(e.target.value);
                            }} 
                            ref={(input) => {this.searchbarinput = input; }}/>
                    </div>
                    <div className="mx-0 px-0" style={{'width': '10%', 'right': '0px'}}>
                        <FontAwesomeIcon className="settings-btn" icon={faCog} size="lg" onClick={(e) => this.props.onSettings()}/>
                    </div>
                </div>
            </div>
        );
    }

    componentDidMount() {
        this.searchbarinput.focus();
    }
}
export default SearchBar;