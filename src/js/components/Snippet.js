import React from 'react';
import { Component } from 'react';
import '../../css/Snippet.css';

const maxContentLength = 62;

class Snippet extends Component{
    render() {
        let content = this.props.snippet.content;
        if(content.length >= maxContentLength) {
            content = content.substring(0, maxContentLength) + '...'
        }
        let classname = 'row border-bottom p-2 h-10';
        if(this.props.selected) {
            classname += ' snippet-selected';
        } else {
            classname += ' snippet';
        }
        console.log(classname)
        return(
            <div ref={(div) => {this.snippetdiv = div;}} className={classname} onClick={(e) => {this.props.onClick(this.props.snippet)}}>
                <div className='col'>
                    <div className='snippet-title'>{this.props.snippet.title}</div>
                    <div className={this.props.selected ? 'snippet-content-selected' : 'snippet-content'}>{content}</div>
                </div>
            </div>
        );
    }
}
export default Snippet;