const config = {
    shortcuts: [
        {
            name: "New snippet",
            description: "Keyboard shortcut for creating a new snippet.",
            readable_shortcut: "Cmd + N",
            keyEvent: (e) => {
                if(e.key == 'n' && e.metaKey) {
                    return true;
                } else {
                    return false;
                }
            },
            execShortcut: (app_component, keyDownEvent) => {
                app_component.setState({mode: 'new'});
            }
        },
        {
            name: 'Focus searchbar',
            description: "Focus the cursor at the searchbar to type.",
            readable_shortcut: 'Cmd + L',
            keyEvent: (e) => {
                if(e.key == 'l' && e.metaKey) {
                    return true;
                } else {
                    return false;
                }
            },
            execShortcut: (app_component, keyDownEvent) => {
                document.getElementById('query').focus();
            }
        }
    ]
}

class ShortcutService {
    constructor() {
        console.log(typeof(config.shortcuts[0].keyEvent));
    }

    handleEvent(app_component, keyDownEvent) {
        console.log('app:', app_component.constructor.name);
        for(let i = 0; i < config.shortcuts.length; i++) {
            if(config.shortcuts[i].keyEvent(keyDownEvent)) {
                config.shortcuts[i].execShortcut(app_component, keyDownEvent);
            }
        }
    }

    getShortcuts() {
        return config.shortcuts;
    }


}
export default ShortcutService;