var remote = window.require('electron').remote;
var fs = remote.require('fs');

class ImportService {
    constructor() {

    }

    readFile(path, callback) {
        let parsed; 
        try {
            parsed = JSON.parse(fs.readFileSync(path, 'utf8'));
            callback(null, parsed);
        } catch(err) {
            parsed = null;    
            callback(err, parsed);
        }
    }
}
export default ImportService;