var remote = window.require('electron').remote;
var fs = remote.require('fs');
const path = require('path');
const userconfig_path = path.join(remote.app.getAppPath('userData'), '/src/userconfig/snippets.json');

class SnippetService {
    constructor() {
        this.load();     
    }

    load() {
        let parsed; 
        try {
            parsed = JSON.parse(fs.readFileSync(userconfig_path, 'utf8'));
        } catch(err) {
            parsed = null;    
        }
        if(parsed) {
            this.snippets = parsed.snippets;
            this.idcounter = parsed.idcounter;
        } else {
            this.snippets = null;
            this.idcounter = 0;
        }
        return parsed;
    }

    save(snippets, idcounter, callback) {
        console.log(snippets);
        let dataToSave = JSON.stringify({snippets: snippets, idcounter: idcounter});
        try {
            fs.writeFileSync(userconfig_path, dataToSave);    
            this.load();
            callback(null);
        } catch(err) {
            console.error(err);
            callback(err);
        }
    }

    import(snippets, callback) {
        this.load();
        let current_snippets = this.snippets;
        let idcounter = this.idcounter + snippets.length;
        if(current_snippets != null) {
            let snippets_to_save = current_snippets;
            let current_max_id = this.idcounter + 1;
            for(let i = 0; i < snippets.length; i++) {
                snippets[i].id = current_max_id;
                current_max_id += 1;
                snippets_to_save.push(snippets[i]);
            }
            this.save(snippets_to_save, idcounter, callback);
        }
    }
    
    newSnippet(snippet, callback) {
        this.load();
        let snippets = this.snippets;
        snippet.id = this.idcounter + 1;
        if(snippets != undefined && snippets != null) {
            snippets.unshift(snippet); // add new snippet to start of array
        } else {
            snippets = [snippet];
        }
        this.snippets = snippets;
        this.idcounter = this.idcounter + 1;
        this.save(this.snippets, this.idcounter, (err) => {
            if(err) {
                callback(err);
            } else {
                callback(null);
            }
        })
    }
    
    updateSnippet(snippet, callback) {
        this.load();
        for(let i = 0; i < this.snippets.length; i++) {
            if(this.snippets[i].id == snippet.id) {
                this.snippets[i] = snippet;
                break;
            }
        }
        
        this.save(this.snippets, this.idcounter, (err) => {
            if(err) {
                callback(err);
            } else {
                callback(null)
            }
        });
    }
    
    deleteSnippet(snippet, callback) {
        this.load();
        console.log('before', this.snippets, snippet);
        for(let i = 0; i < this.snippets.length; i++) {
            if(this.snippets[i].id == snippet.id) {
                this.snippets.splice(i, 1);
                break;
            }
        }
        console.log('after', this.snippets, snippet);
        this.save(this.snippets, this.idcounter, (err) => {
            if(err) {
                callback(err);
            } else {
                callback(null)
            }
        });
    }

    __searchFunction(snippet, query) {
        if(snippet.title.toLowerCase().includes(query.toLowerCase()) || snippet.content.toLowerCase().includes(query.toLowerCase())) {
            return true;
        } else {
            return false;
        }
    }
    
    getCurrentSnippets(query) {
        let data = this.load();
        if(data) {
            let snippets = data.snippets;
            if(query != undefined && query != null && query != '') {
                // A query exists, perform search
                let results = [];
                for(let i = 0; i < snippets.length; i++) {
                    if(this.__searchFunction(snippets[i], query)) {
                        results.push(snippets[i]);
                    }
                }
                console.log(query, snippets, results);
                return results;
            } else {
                return data.snippets;
            }
        } else {
            return [];
        }
    }
}
export default SnippetService;
