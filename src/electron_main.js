const Electron = require('electron');
const { app, Menu, Tray, BrowserWindow, screen, globalShortcut } = require('electron');
const { ipcMain } = require('electron')
const isDev = require('electron-is-dev');
const WINDOW_WIDTH = 600;
const WINDOW_HEIGHT = 456;
const DEV_TOOLS_WIDHT = 400;
const log = require('electron-log');
const path = require('path');
const { electron } = require('process');
const { ELOOP } = require('constants');
let AutoLaunch = require('auto-launch');

let appAutoLauncher = new AutoLaunch({
	name: "Snippets",
	path: "/Applications/Snippets.app"
});
console.log(appAutoLauncher);
appAutoLauncher.enable();

appAutoLauncher.isEnabled().then((isEnabled) => {
	if(isEnabled) {
		return;
	}
	appAutoLauncher.enable();
}).catch((err) => {
	log.error(err);
});

let window;
let tray;
let STATE = {
	WINDOW_VISIBLE: false,
	LOADED: false,
	DEV_TOOLS: false,
	COMPLETELY_QUIT: false
};

function placeWindow(win, trayPosition) {
	if(trayPosition == undefined || trayPosition == null) {
		if(STATE.POSITION == undefined) { 
			// Window has not been moved manually before, use center of current screen
			const {x, y} = screen.getCursorScreenPoint();
			const currentDisplay = screen.getDisplayNearestPoint({ x, y });
			let centerX = currentDisplay.bounds.x + (currentDisplay.bounds.width / 2) - WINDOW_WIDTH / 2; 
			let centerY = currentDisplay.bounds.y + (currentDisplay.bounds.height / 2) - WINDOW_HEIGHT / 2;
			win.setPosition(Math.floor(centerX), Math.floor(centerY));
			STATE.POSITION = {
				X: Math.floor(centerX), 
				Y: Math.floor(centerY)
			};
		} else {
			// Window has been moved manually before
			// Check that the position at last move, is also on the same screen that the cursor is on
			const cursor_point = screen.getCursorScreenPoint();
			
			const displayWithCursor = screen.getDisplayNearestPoint({x: cursor_point.x, y: cursor_point.y});
			const displayAtLastMove = screen.getDisplayNearestPoint({x: STATE.POSITION.X, y: STATE.POSITION.Y});
			if(displayAtLastMove.id != displayWithCursor.id) {
				// The cursor is on another display from what it was at the last move.
				// Show window at cursor screen
				let currentDisplay = displayWithCursor;
				let centerX = currentDisplay.bounds.x + (currentDisplay.bounds.width / 2) - WINDOW_WIDTH / 2; 
				let centerY = currentDisplay.bounds.y + (currentDisplay.bounds.height / 2) - WINDOW_HEIGHT / 2;
				win.setPosition(Math.floor(centerX), Math.floor(centerY));
				STATE.POSITION = {
					X: Math.floor(centerX), 
					Y: Math.floor(centerY)
				};
			} else {
				// Cursor is on same screen as before. Show window at state posision
				win.setPosition(STATE.POSITION.X, STATE.POSITION.Y);
			}
		}
	} else {
		let {x, y} = trayPosition; // Position of the tray icon that was clicked
		// Want to draw the window in the upper right of the screen, such that the middle of the window width is right underneith the tray icon
		const currentDisplay = screen.getDisplayNearestPoint({ x, y });
		let topbarHeight = currentDisplay.workArea.y;
		let drawX = x - (WINDOW_WIDTH / 2);
		win.setPosition(drawX, Math.floor(1.5 * topbarHeight));
	}
	
	win.show();
	STATE.WINDOW_VISIBLE = true;
	win.focus();
}

function hideWindow(win) {
	win.hide();
	win.webContents.closeDevTools();
	Menu.sendActionToFirstResponder('hide:');
	STATE.WINDOW_VISIBLE = false;
}

function createWindow () {
	// Create the browser window.
	app.dock.hide();
	const win = new BrowserWindow({
		width: WINDOW_WIDTH,
		height: WINDOW_HEIGHT,
		frame: false,
		alwaysOnTop: true,
		webPreferences: {
			nodeIntegration: true,
			nodeIntegrationInWorker: true
		}
	});
	
	win.setAlwaysOnTop(true, 'floating')
	win.setVisibleOnAllWorkspaces(true)
	win.setFullScreenable(false)
	// app.dock.show(); // With this enabled, the app dock icon will not be shown
	
	win.loadFile('./dist/index.html')
	placeWindow(win, null);
	window = win;
	globalShortcut.register('Alt+X', () => {
		if(STATE.WINDOW_VISIBLE) {
			hideWindow(win);
		} else {
			placeWindow(win, null);
		}
	});

	if(isDev) {
		globalShortcut.register('CommandOrControl+D', () => {
			STATE.DEV_TOOLS = !STATE.DEV_TOOLS;
			if(STATE.DEV_TOOLS) {
				win.webContents.openDevTools({mode: 'right'});
				win.setSize(WINDOW_WIDTH + DEV_TOOLS_WIDHT, WINDOW_HEIGHT);
			} else {
				win.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
				win.webContents.closeDevTools();
			}
		});
	}

	// globalShortcut.register('Command+Q', () => {
	// 	// Do nothing
	// 	// hideWindow(win);
	// });
	
	// globalShortcut.register('Command+W', () => {
	// 	hideWindow(win);
	// });

	win.on('moved', () => {
		let pos = win.getPosition();
		STATE.POSITION = {
			X: pos[0],
			Y: pos[1]
		};
	});

	win.on('close', (event) => {
		if (process.platform !== 'darwin') {
			app.quit()
		} else {
			if(STATE.COMPLETELY_QUIT == true) {
				app.quit();
			} else {
				event.preventDefault(); // Do not let app quit/close, but hide window instead. To quit app, must right click tray icon and select quit.
				hideWindow(win);
			}
		}
	});

	win.on('blur', () => {
		if(STATE.LOADED == true) {
			hideWindow(win);
		}
	});

	ipcMain.on('app-loaded', (event, arg) => {
		// placeWindow(win);
		STATE.LOADED = true;
		event.returnValue = "does-not-matter";
	});
	
	ipcMain.on('close-app', (event, arg) => {
		if (process.platform !== 'darwin') {
			win.close();
			//app.quit()
		} else {
			hideWindow(win);
			// win.hide();
			// if(isDev) {
			// 	win.webContents.closeDevTools();
			// }
		}
	});
	tray = new Tray(path.join(__dirname, '../assets/tray-icon-19x19.png'));
	const contextMenu = Menu.buildFromTemplate([
		{ label: 'Quit Snippets Completely', click: () => {
			STATE.COMPLETELY_QUIT = true;
			app.quit();
		}}
	])
	tray.setToolTip('Snippets')
	tray.on('right-click', () => {
		tray.popUpContextMenu(contextMenu);
	});
	tray.on('click', (event, bounds, position) => {
		placeWindow(win, bounds);
	});
}

app.on('activate', () => {
	if (BrowserWindow.getAllWindows().length === 0) {
		// No window exists, create new
		createWindow()
	} else {
		// Window already exists, but may be hidden. Show window. 
		window.show();
	}
});

app.on('ready', () => {
	createWindow();
});
