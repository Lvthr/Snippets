<h1 align="center">
  <a href="assets/icon.png"><img src="assets/icon.png" alt="Snippets" width="100"></a>
  <br>
  Snippets
</h1>

Snippets is a lightweight desktop application created with React and Electron, allowing you to create, edit and delete text *snippets* you use often. The application also allowes you to search for snippets by their title, and when selected the snippets contents are copied to your machines clipboard. 

![Snippets screenshot](SnippetsGif.gif)